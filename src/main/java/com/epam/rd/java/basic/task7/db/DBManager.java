package com.epam.rd.java.basic.task7.db;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


import com.epam.rd.java.basic.task7.db.entity.*;
import org.apache.ibatis.jdbc.ScriptRunner;


public class DBManager {

	private static DBManager instance;
	private Connection con;

	public Connection getCon() {
		return con;
	}

	public static synchronized DBManager getInstance() {
		if (instance == null) instance = new DBManager();
		return instance;
	}

	private DBManager() {
		try {
			Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
			con = DriverManager.getConnection("jdbc:derby:memory:testdb;create=true");
			System.out.println("Connected.....");
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public List<User> findAllUsers() throws DBException {

		List<User> users = new ArrayList<>();
		ResultSet rs = null;
		Statement statement = null;
		int id = 0;
		String login = null;

		try {
			statement = con.createStatement(
					ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_UPDATABLE
			);
			rs = statement.executeQuery("SELECT * FROM users");
			while (rs.next()){
				id = rs.getInt("id");
				login = rs.getString("login");
				User user = new User(login);
				user.setId(id);
				users.add(user);
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(),e);
		}

		return users;
	}															///DONE 4 derbyDB!

	public boolean insertUser(User user) throws DBException {

		ResultSet rs = null;
		Statement statement = null;
		int lastId = 0;

		try {
			statement = con.createStatement(
					ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_UPDATABLE
			);
			rs = statement.executeQuery("SELECT * FROM users");

			while (rs.next()){
				if (lastId < rs.getInt("id")) lastId = rs.getInt("id");
			}
			user.setId(lastId+1);																						// new USER set ID
		} catch (SQLException e) {
			throw new DBException(e.getMessage(),e);
		}

		try {
			con.createStatement().executeUpdate("INSERT INTO users VALUES (DEFAULT, '"+ user.getLogin() +"')");
			return true;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(),e);
		}
	}														//DONE 4 derbyDB!

	public boolean deleteUsers(User... users) throws DBException {
		try {
			for(User user : users){
				con.createStatement()
						.executeUpdate("DELETE FROM users WHERE login = '"+user.getLogin()+"'");
			}
			return true;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(),e);
		}
	}													//DONE 4 derbyDB!!

	public User getUser(String login) throws DBException {

		ResultSet rs = null;
		Statement statement = null;
		int id = 0;
		User user = new User(login);

		try {
			statement = con.createStatement(
					ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_UPDATABLE
			);
			rs = statement.executeQuery("SELECT * FROM users WHERE login = '" + login + "'");
			while (rs.next()){
				id = rs.getInt("id");
			}
			user.setId(id);
		} catch (SQLException e) {
			throw new DBException(e.getMessage(),e);
		}
		return user;
	}															//DONE 4 derbyDB!!

	public Team getTeam(String name) throws DBException {
		ResultSet rs = null;
		Statement statement = null;
		Team team = new Team(name);

		try {
			statement=con.createStatement(
					ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_UPDATABLE
			);
			rs = statement.executeQuery("SELECT * FROM teams WHERE name = '"+name+"'");
			while (rs.next()){
				team.setId(rs.getInt("id"));
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(),e);
		}
		return team;
	}															//DONE 4 derbyDB!!

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		ResultSet rs = null;
		Statement statement = null;
		int id = 0;
		String name = null;

		try {
			statement = con.createStatement(
					ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_UPDATABLE
			);
			rs = statement.executeQuery("SELECT * FROM teams");
			while (rs.next()){
				id = rs.getInt("id");
				name = rs.getString("name");
				Team team = new Team(name);
				team.setId(id);
				teams.add(team);
			}
		} catch (SQLException e) {
			throw new DBException(e.getMessage(),e);
		}

		return teams;
	}															//DONE 4 derbyDB!!

	public boolean insertTeam(Team team) throws DBException {
		ResultSet rs = null;
		Statement statement = null;
		int lastId = 0;

		try {
			statement = con.createStatement(
					ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_UPDATABLE
			);
			rs = statement.executeQuery("SELECT * FROM teams");

			while (rs.next()){
				if (lastId < rs.getInt("id")) lastId = rs.getInt("id");
			}
			team.setId(lastId+1);																						// new USER set ID
		} catch (SQLException e) {
			throw new DBException(e.getMessage(),e);
		}

		try {
			con.createStatement().executeUpdate("INSERT INTO teams VALUES (DEFAULT, '"+ team.getName() +"')");
			return true;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(),e);
		}
	}														//DONE 4 derbyDB!!

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		String insertString = "INSERT INTO users_teams VALUES ( ?, ?)";

		try (PreparedStatement insertStatement = con.prepareStatement(insertString)) {
			con.setAutoCommit(false);
			for(Team team : teams){
				insertStatement.setInt(1, user.getId());
				insertStatement.setInt(2, team.getId());
				insertStatement.executeUpdate();

			}
			con.commit();
			return true;

		} catch (SQLException e) {
			try {
				con.rollback();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			throw new DBException(e.getMessage(),e);
		} finally {
			try {
				con.setAutoCommit(true);
			} catch (SQLException e) {
				throw new DBException(e.getMessage(),e);
			}
		}
	}									//DONE 4 derbyDB!!
	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		List<Integer> teamIds = new ArrayList<>();
		ResultSet rs = null;
		Statement statement = null;
		int userId = user.getId();

		try{
			statement = con.createStatement(
					ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_UPDATABLE
			);
			rs = statement.executeQuery("SELECT * FROM users_teams WHERE user_id = "+userId);
			while(rs.next()){
				teamIds.add(rs.getInt("team_id"));
			}
			for(Integer teamId : teamIds){
				rs = statement.executeQuery("SELECT * FROM teams WHERE id = "+ teamId);
				while (rs.next()){
					Team team = new Team(rs.getString("name"));
					team.setId(rs.getInt("id"));
					teams.add(team);
				}
			}

		} catch (SQLException e) {
			throw new DBException(e.getMessage(),e);
		}

		return teams;
	}													//DONE 4 derbyDB!!

	public boolean deleteTeam(Team team) throws DBException {

		try {
			con.createStatement().executeUpdate("DELETE FROM teams WHERE name = '"+team.getName()+"'");
			return true;
		} catch (SQLException e) {
			throw new DBException(e.getMessage(),e);
		}
	}														//DONE 4 derbyDB!!

	public boolean updateTeam(Team team) throws DBException {
		String updateString = "UPDATE teams SET name = ? WHERE id = ?";
		try (PreparedStatement updateStatement = con.prepareStatement(updateString)){

			updateStatement.setString(1, team.getName());
			updateStatement.setInt(2, team.getId());
			updateStatement.executeUpdate();
			//System.out.println(team.getName());
			//con.createStatement().executeUpdate("UPDATE teams SET name =" + team.getName() + "WHERE id =" + team.getId());
		} catch (SQLException e) {
			throw new DBException(e.getMessage(),e);
		}


		return false;
	}

}
