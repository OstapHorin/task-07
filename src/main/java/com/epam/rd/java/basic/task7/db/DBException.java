package com.epam.rd.java.basic.task7.db;

public class DBException extends Exception {
	String message;
	Throwable cause;

	public DBException(String message, Throwable cause) {
		this.cause=cause;
		this.message=message;
	}

}
